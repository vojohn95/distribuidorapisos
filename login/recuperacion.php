<?php
include '../conexion/conexion.php';
include '../composer/vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.css">
    <title>Facturador OCE</title>
</head>
<body>
<?php
if (isset($_POST['correo'])) {
    $nick = strip_tags(htmlentities(trim($_POST['correo'])));
    //echo $nick;

    $con = "SELECT  email , name , password , id  FROM users WHERE email = '" . $nick . "'";
    $consulta = mysqli_query($mysqli, $con) or die('Error al buscar en la base de datos.');


    if (mysqli_num_rows($consulta) > 0) {
        while ($f = mysqli_fetch_assoc($consulta)) {
            $para = $f['email'];
            $user_id = $f['id'];
            $user = $f['name'];
        }

        //echo "4";
        $rand_part = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789" . uniqid());
//echo $rand_part;
//echo $rand_part;
        //echo "5";

//echo "7";

        $up = "UPDATE users SET users.remember_token= '" . $rand_part . "' WHERE id=" . $user_id;
//echo "6";
        $idtest = $f['id'];
//var_dump($idtest);

        $consul = mysqli_query($mysqli, $up);
//echo $user_id;
        //echo $consul;
        $titulo = 'Recuperacion de cuenta';
//$mensaje   = 'Hola ' .$f['nick'] .  ' este es tu token: '. $rand_part;
//$url = 'http://'.$_SERVER["SERVER_NAME"].'/GestionArchivos/login/cambia.php?user_id='.$user_id.'&token='.$rand_part;
        $url = 'http://' . $_SERVER["SERVER_NAME"] . '/login/cambia.php?user_id=' . $user_id . '&token=' . $rand_part;
//$url = 'http://validador-dev.central-mx.com/login/cambia.php?user_id='.$user_id.'&token='.$rand_part;
//echo "8";
        $mensaje = 'Hola <br>' . $f['name'] . 'Por favor de click en el siguiente enlace para cambiar su contraseña y recuperar su acceso:  ' . $url . ' <br> Este link tiene una validez de 24 horas. ';


        $mail = new PHPMailer(true);
        $mail->From = "no-replay@central-mx.com";
        $mail->FromName = "Central operadora de estacionamientos";
        $mail->Subject = "Recuperacion de cuenta";
        $mail->Body = $mensaje;
        $mail->AddAddress($para, $user);
        $mail->IsHTML(true);


        if ($mail->Send()) {
            $tituloSWAL = 'Mensaje enviado';
            $mensajeSWAL = 'Si no lo encuentra por favor revise su bandeja de spam o correo no deseado';
            $t = 'success';
            $dir = '../index';
            //echo "10";
        } else {
            $tituloSWAL = 'Mensaje no pudo ser enviado';
            $mensajeSWAL = 'Correo no registrado Contacte a su administrador';
            $t = 'error';
            $dir = '../index';
            //echo "11";
        }
    } else {
        $tituloSWAL = 'Mensaje no pudo ser enviado';
        $mensajeSWAL = 'Correo no registrado';
        $t = 'error';
        $dir = '../index';
    }
}else{
    $tituloSWAL = 'Error';
    $mensajeSWAL = 'Utiliza el formulario';
    $t = 'error';
    $dir = '../index';
}
?>


<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
<script>
    swal({
        title: '<?php echo $tituloSWAL ?>',
        text: '<?php echo $mensajeSWAL ?>',
        type: '<?php echo $t ?>',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'ok'
    }).then(function () {
        location.href = '<?php echo $dir ?>';
    });

    $(document).click(function () {
        location.href = '<?php echo $dir ?>';
    });

    $(document).keyup(function (e) {
        if (e.which == 27) {
            location.href = '<?php echo $dir ?>';
        }
    });

</script>

</body>
</html>

