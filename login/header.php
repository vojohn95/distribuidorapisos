<?php

?>

<!DOCTYPE html>
<html>
<head>
    <title>Panel de administración</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<nav>
    <div class="nav-wrapper black">
        <a class="brand-logo center">Panel de control</a>
        <ul class="left hide-on-med-and-down">
            <li><a href="../">Regresar a pagina principal</a></li>
        </ul>
    </div>
</nav>


