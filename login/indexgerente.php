<?php
include '../conexion/conexion.php';

//añadido para evitar que la session no se abra
if(!isset($_SESSION))
{
    session_start();

}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $mail = strip_tags(htmlentities(trim($_POST['mail_log'])));
    $pass = strip_tags(htmlentities(trim($_POST['pass_log'])));
    $candado = ' ';
    //Esta funcion busca caracteres
    $str_u = strpos($mail,$candado);//usuario
    $str_p = strpos($pass,$candado);//password

    if (!isset($mail) || !isset($pass) )
    {
        //echo "Ingrese las credenciales";
        header('location: ../extend/alerta?msj=Ingrese sus credenciales correctas&c=index&p=gerentes&t=error');
    }else{

        $query = "SELECT id, name, email, password FROM `central_users` WHERE email = '".$mail."' ";
        $consulta = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');
        $row = mysqli_num_rows($consulta);
        while ($f=mysqli_fetch_assoc($consulta)) {
            echo "<br>";
            $hash = $f['password'];
            $dbid = $f['id'];
            $dbmail = $f['email'];
            $dbname = $f['name'];
            echo "<br>";
        }

        if($mail != $dbmail || !isset($dbid) || $row != 1 )
        {
            //echo "el nombre o la contraseña son incorrectos";
            header('location: ../extend/alerta?msj=El nombre o la contrase\u00f1a es incorrecto&c=index&p=gerentes&t=error');
        }

        if (password_verify($pass, $hash)) {
            //echo '¡La contraseña es válida!';
            $ID     = $dbid;
            $NAME   = $dbname;
            $MAIL   = $dbmail;

            $_SESSION['id'] = $ID;
            $_SESSION['name'] = $NAME;
            $_SESSION['mail']= $MAIL;

            //header('location: ../extend/alerta.php?msj=Bienvenido!&c=menu&p=menu&t=success');
            header('Location: ../gerencia/index');


        } else {


            //echo 'La contraseña no es válida.';
            header('location: ../extend/alerta?msj=La contrase\u00f1a es incorrecta&c=index&p=gerentes&t=error');

        }

    }//Termina else de ingrese sus credenciales correctar


}//termina if de request
else{
    echo "utiliza el formulario para continuar";
    header('location: ../extend/alerta?msj=Utiliza el formulario para continuar!&c=index&p=gerentes&t=error');
}

?>
