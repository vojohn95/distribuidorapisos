<?php
include '../conexion/conexion.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

$rfc = htmlentities(trim(strtoupper($_POST['rfcreg'])));
echo "<br>";
$razonsocial = htmlentities(trim(strtoupper($_POST['razonsocialreg'])));
echo "<br>";
$correo = htmlentities(trim($_POST['correoreg']));
echo "<br>";
$pass = htmlentities(trim($_POST['passreg']));
echo "<br>";


    if (empty($rfc) || empty($razonsocial) || empty($correo) || empty($pass)) {
        //echo "Hay un campo sin especificar";
        header('location:../extend/alerta?msj=Hay un campo sin especificar&c=index&p=index&t=error');
        exit;
    }//termina comprobacion de envio

    $rfclen = strlen($rfc);
    $contra = strlen($pass);

    if ($rfclen < 11 || $rfclen > 13) {
        echo "El rfc debe ser de entre 12 y 13 caracteres";
        header('location:../extend/alerta?msj=El rfc debe ser de entre 12 y 13 caracteres&c=index&p=index&t=error');
        //header('location: ../extend/alerta.php?msj=El nick debe contener entre 8 y 15 caracteres&c=us&p=in&t=error');
        exit;
    }//termina rfc

    if ($contra < 8 || $contra >15) {
        //echo "la contraseña debe contener entre 8 y 15 caracteres";
        header('location:../extend/alerta?msj=El password debe contener entre 8 y 15 caracteres&c=index&p=index&t=error');
        //header('location: ../extend/alerta.php?msj=la contraseña debe contener entre 8 y 15 caracteres&c=us&p=in&t=error');
        exit;
    }//termina if contraseña

    if (!empty($correo)) {
        if (!filter_var($correo,FILTER_VALIDATE_EMAIL)) {
            echo "el email no es valido";
            header('location:../extend/alerta?msj=El email no es valido&c=index&p=index&t=error');
            //header('location: ../extend/alerta.php?msj=El email no es valido&c=us&p=in&t=error');
            exit;
        }
    }//termina if correo


    $sel = "SELECT email, password FROM users WHERE email = '$correo' ";
    $consulta2 = mysqli_query($mysqli, $sel) or die('Error al buscar en la base de datos.');
    $row = mysqli_num_rows($consulta2);
    while ($row = mysqli_fetch_array($consulta2, MYSQLI_ASSOC)) {
        $pass_null = $row['password'];

        var_dump($pass_null);

    }

    if ($pass_null == "sin_contrasena"){
        $passs = password_hash($pass, PASSWORD_DEFAULT, [15]);
        $timestamp = date("Y-m-d H:i:s");
        $users_pension = "UPDATE users SET name = '".$razonsocial."', password ='".$passs."' WHERE email = '".$correo."' ";
        $mysql_update = mysqli_query($mysqli, $users_pension) or die('Error al actualizar.');
        header('location: ../extend/alerta?msj=Usuario registrado con exito&c=index&p=index&t=success');
    }else{
        if ($row >= 1){
            header('location:../extend/alerta?msj=El correo que introdujo ya esta registrado&c=index&p=index&t=error');
            exit;
        }

        $passs = password_hash($pass, PASSWORD_DEFAULT, [15]);

        //$passs = password_hash($contra, PASSWORD_DEFAULT)."\n";
        $timestamp = date("Y-m-d H:i:s");

        $sql = "INSERT INTO users (id, name, email, password ) VALUES (NULL,'".$razonsocial."','".$correo."','".$passs."')";
        //echo $sql;
        //echo "<br>";
        if ($consulta = mysqli_query($mysqli, $sql)) {
            $selmax = "SELECT MAX(id) FROM users";
            $consultamax = mysqli_query($mysqli, $selmax);
            $fmax = mysqli_fetch_assoc($consultamax);
            $idmax = $fmax['MAX(id)'];

            $sqlinfo = "INSERT INTO user_infos (id, RFC , Razon_social, created_at, id_cliente ) VALUES (NULL,'".$rfc."','".$razonsocial."','".$timestamp."','".$idmax."' )";
            if ($consulta_info = mysqli_query($mysqli, $sqlinfo)) {
                header('location: ../extend/alerta?msj=Usuario registrado con exito&c=index&p=index&t=success');
            }else{
                //echo "usuario no se registro";
                header('location:../extend/alerta.php?msj=El usuario no se registro&c=index&p=index&t=error');

                //printf("Errormessage: %s\n", $mysqli->error);
            }

            //header('location: ../extend/alerta.php?msj=El usuario ha sido registrado&c=us&p=in&t=success');
        }else{
            // echo "Usuario no se registro";
            header('location:../extend/alerta?msj=El usuario ya existe&c=index&p=index&t=error');

            //printf("Errormessage: %s\n", $mysqli->error);
            //header('location: ../extend/alerta.php?msj=El usuario no pudo ser registrado&c=us&p=in&t=error');

        }
    }

}//termina if principal
else{
   // echo "utiliza el formulario";
    header('location: ../extend/alerta?msj=Utiliza el formulario&c=us&p=in&t=error');
}