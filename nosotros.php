<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Bienvenidos Distribuidora de Pisos. Somos una empresa 100% mexicana, dedicada a la decoración y ambientación de interiores con una amplia experiencia ...">
    <meta content="linoleums, alfombras, loseta vinílica, pisos laminados, y productos para diseño de interiores."/>
    <meta content="Venta especializada de pisos" property="og:title">
    <meta content="http://www.distribuidoradepisos.com.mx/" property="og:url">
    <meta content="Distribuidora de pisos" property="og:site_name">
    <meta content="website" property="og:type">
    <meta name="author" content="Esquitechs">
    <meta name="copyright" content="Esquitechs" />
    <meta property="og:locale" content="es_MX">
    <meta property="og:image" content="https:///distribuidoradepisos.com.mx/images/logotipo.gif">
    <meta property="og:description"
          content="Bienvenidos Distribuidora de Pisos. Somos una empresa 100% mexicana, dedicada a la decoración y ambientación de interiores con una amplia experiencia ...">
    <meta property="og:site_name" content="Distribuidora de pisos">
    <meta name="Keywords" content="pisos y azulejos, pisos, pisos cerámicos, pisos para bańos, azulejos para piso, azulejos, loseta, pisos, "/>
    <meta name="robots" content="index,follow"/>
    <meta http-equiv="expires" content="3600"/>
    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">

    <!-- SITE TITLE -->
    <title>Distribuidora de pisos | Empresa de desarrollo de software</title>

    <link rel=”alternate” hreflang=”es-MX” href=”http://distribuidoradepisos.com.mx/”/>
    <link rel=”canonical” href=”https://esquitechs.com/"/>
    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">


    <!-- Favicons
      ================================================== -->
    <link rel="shortcut icon" href="favicon.png">

    <style>@charset "UTF-8";@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);@import url(https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900&subset=latin,vietnamese);@-ms-viewport{width:auto!important}html{overflow-x:hidden}a{text-decoration:none}body{font-family:'Roboto',sans-serif;font-size:15px;line-height:25px;color:#666;overflow:hidden}td{vertical-align:middle!important}html,body{min-height:100%}@media only screen and (max-width:479px){body{font-size:14px}}</style>
</head>
<body>
<!--<div id="switcher">
    <span class="custom-close"></span>
   <span class="custom-show"></span>
    <div class="mCustomScrollbar" data-mcs-theme="minimal-dark">
           <a href="#" class="ot-btn btn-main-color block-btn">Buy Now</a>

           <div class="clearfix"></div>

           <span class="sw-title">Main Colors:</span>
           <ul id="tm-color">
               <li class="color1"></li>
               <li class="color2"></li>
               <li class="color3"></li>
               <li class="color4"></li>
               <li class="color5"></li>
               <li class="color6"></li>
               <li class="color7"></li>
           </ul>
           <div class="clearfix"></div>

           <span class="sw-title">Header Layout</span>
           <select name="switcher" id="de-header-layout">
               <option value="opt-1" selected="">Hide Top Bar</option>
               <option value="opt-2">Show Top Bar</option>
           </select>
           <div class="clearfix"></div>


           <span class="sw-title">Menu Seperator Style</span>
           <select name="switcher" id="de-menu">
               <option value="opt-1" selected="">Line Throu Separator</option>
               <option value="opt-2">Line Separator</option>
               <option value="opt-3">Circle Separator</option>
               <option value="opt-4">Square Separator</option>
               <option value="opt-5">Plus Separator</option>
               <option value="opt-6">Strip Separator</option>
               <option value="opt-0">No Separator</option>
           </select>

           <div class="clearfix"></div>

           <span class="sw-title">Hover Header Link Effect</span>
           <select name="switcher" id="de-menu-eff">
               <option value="opt-1" selected="">Line Through Effect</option>
               <option value="opt-2">Background Effect</option>
               <option value="opt-3">Text Color</option>
               <option value="opt-4">Line Expand</option>
           </select>

           <div class="clearfix"></div>


           <span class="sw-title">HomePage Layout</span>
           <ul class="demo-homepage">
               <li>
                   <a href="index">
                       <img src="switcher\images\home_1.webp" alt="">
                   </a>
                   <img src="switcher\images\home_1.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_2">
                       <img src="switcher\images\home_2.webp" alt="">
                   </a>
                   <img src="switcher\images\home_2.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_3">
                       <img src="switcher\images\home_3.webp" alt="">
                   </a>
                   <img src="switcher\images\home_3.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_4">
                       <img src="switcher\images\home_4.webp" alt="">
                   </a>
                   <img src="switcher\images\home_4.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_5">
                       <img src="switcher\images\home_5.webp" alt="">
                   </a>
                   <img src="switcher\images\home_5.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_6">
                       <img src="switcher\images\home_6.webp" alt="">
                   </a>
                   <img src="switcher\images\home_6.webp" class="popup-demo-homepage" alt="Image">
               </li>
           </ul>
           </div>
</div>-->
<!-- End Switcher Color -->
<div class="mobile-menu-first">
    <div id="mobile-menu" class="mobile-menu">
        <div class="header-mobile-menu">
            <a class="text-cap hidden-xs">TEL:(55) 56870562/(55) 56870623 /(55) 55431941</a>
            <div class="mm-toggle">
                <span aria-hidden="true" class="icon_close"></span>
            </div>
        </div> <!-- Mobile Menu -->
        <div class="mCustomScrollbar light" data-mcs-theme="minimal-dark">

            <ul>
                <li class="has-sub"><a href="index"><span>Inicio</span></a>
                </li>
                <li class="has-sub"><a href="productos"><span>Productos</span></a>
                </li>
                <li><a href="servicios"><span>Servicios</span></a></li>
                <li><a href="nosotros"><span>¿Quienes somos? </span></a></li>

                <!--<li><a href="blogList"><span>Blog</span></a>
                    <ul>
                        <li><a href="blogGrid_2_col">Grid 2 Columns</a></li>
                        <li><a href="blogGrid_3_col">Grid 3 Columns</a></li>
                        <li><a href="blogGrid_4_col">Grid 4 Columns</a></li>
                        <li><a href="blogList">Blog List</a></li>
                        <li><a href="blogDetail">Blog Detail</a></li>
                    </ul>
                </li>
                <li><a href=""><span>Pages</span></a>
                    <ul>
                        <li><a href="elements">Element</a></li>
                        <li><a href="typography">Typography</a></li>
                        <li><a href="404">404 Page</a></li>
                        <li><a href="commingsoon">Comming Soon</a></li>
                    </ul>
                </li>
                <li><a href="shop_catalog"><span>Shop</span></a>
                    <ul>
                        <li><a href="shop_catalog">Store Catalog</a></li>
                        <li><a href="shop_cart">Shopping Cart</a></li>

                        <li><a href="shop_single">Single Product</a></li>
                    </ul>
                </li>-->
                <li><a href="contacto"><span>contacto</span></a></li>
            </ul>
        </div> <!-- /#rmm   -->
    </div>
</div><!-- End Mobile Menu -->

<div class="modal fade modal-search" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <div class="modal-dialog myModal-search">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <form role="search" method="get" class="search-form">
                    <input class="search-field" placeholder="Search here..." value="" title="" type="search">
                    <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Search-->
<div id="page">
    <div id="skrollr-body">
        <header id="mainmenu" class="header-v1 header-border header-fix header-bg-white"
                data-0="padding:10px;padding-left:40px;padding-right:40px;"
                data-251="padding:10px; padding-left:40px;padding-right:40px;top:0;">
            <div id="info" class="topbar topbar-position topbar-dark hide-topbar" data-0="height:30px"
                 data-251="height:0;">

                <div class="col-md-12">
                    <p class="text-cap hidden-xs">TEL:(55) 56870562/(55) 56870623 /(55) 55431941</p>
                    <p class="text-cap">distridepisos@yahoo.com.mx</p>
                    <!--<div class="language">
                        <a href="#" class="active">EN</a>
                        <a href="#">FR</a>
                        <a href="#">PT</a>
                    </div>-->
                </div>

            </div>
            <div class="left-header">
                <ul class="navi-level-1">
                    <li><a href="index" class="logo"><img src="images\logotipo.gif" style="width: 150%;"
                                                          class="img-responsive" alt="distribuidora de pisos"></a>
                    </li>
                </ul>
            </div><!-- End Left Header -->
            <nav>
                <ul class="navi-level-1 hover-style-2 main-navi">
                    <li class="has-sub"><a href="index"><span>Inicio</span></a>
                    </li>
                    <li class="has-sub"><a href="productos"><span>Productos</span></a>
                    </li>
                    <li><a href="servicios"><span>Servicios</span></a></li>
                    <li><a href="nosotros"><span>¿Quienes somos? </span></a></li>


                    <!--<li class="has-sub"><a href="blogList"><span>Blog</span></a>
                        <ul class="navi-level-2">
                            <li><a href="blogGrid_2_col">Grid 2 Columns</a></li>
                            <li><a href="blogGrid_3_col">Grid 3 Columns</a></li>
                            <li><a href="blogGrid_4_col">Grid 4 Columns</a></li>
                            <li><a href="blogList">Blog List</a></li>
                            <li><a href="blogDetail">Blog Detail</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"><a href=""><span>Pages</span></a>
                        <ul class="navi-level-2">
                            <li><a href="elements">Element</a></li>
                            <li><a href="typography">Typography</a></li>
                            <li><a href="404">404 Page</a></li>
                            <li><a href="commingsoon">Comming Soon</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"><a href="shop_catalog"><span>Shop</span></a>
                        <ul class="navi-level-2">
                            <li><a href="shop_catalog">Store Catalog</a></li>
                            <li><a href="shop_cart">Shopping Cart</a></li>

                            <li><a href="shop_single">Single Product</a></li>
                        </ul>
                    </li>-->
                    <li class="has-sub"><a href="contacto"><span>contactanos</span></a></li>

                </ul>
            </nav><!-- End Nav -->
            <div class="right-header">
                <ul class="navi-level-1 sub-navi seperator-horizonal-line hover-style-4">
                    <li class="header-tel"><a class="tel-header">TEL:(55) 56870562/(55) 56870623 /(55) 55431941</a></li>


                    <!-- Testing Search Box -->
                    <!--<li><a href="#"><span aria-hidden="true" class="icon_bag_alt"></span>
                        </a>
                    </li>
                     <li>
                        <a href="#" data-toggle="modal" data-target="#myModal" id="btn-search" class="reset-btn btn-in-navi"><span aria-hidden="true" class="icon_search"></span></a>
                    </li>-->
                    <li>
                        <a href="#/" class="mm-toggle">
                            <span aria-hidden="true" class="icon_menu"></span>
                        </a>
                    </li>
                </ul>

            </div><!-- End Right Header -->
        </header>
        <!-- End  Header -->
        <!-- End  Header -->
        <section>
            <div class="sub-header sub-header-1 sub-header-about fake-position">
                <div class="sub-header-content">
                    <h2 class="text-cap white-text">¿Quienes somos?</h2>
                    <ol class="breadcrumb breadcrumb-arc text-cap">
                        <li>
                            <a href="index.html">INICIO</a>
                        </li>
                        <li class="active">¿Quienes somos?</li>
                    </ol>
                </div>
            </div>
        </section>
        <!-- End Section Sub Header -->

        <section class="padding padding-bottom-0">
            <div class="container">
                <div class="row">
                    <div class="about-intro">
                        <div class="about-intro-img">
                            <img src="images\About\1.webp" class="img-responsive" alt="Image">
                        </div>
                        <br><br><br>
                        <div class="about-intro-text">
                            <h2 class="text-cap">NOSOTROS</h2>
                            <p>Somos una empresa 100% mexicana, dedicada a la decoración y ambientación de interiores con una amplia experiencia que nos permite asesorarte en la mejor elecciòn de pisos, persianas, cortinas, alfombras y tapices de acuerdo a tu gusto y necesidades.</p>
                            <div class="clearfix mgb20"></div>
                            <h2 class="text-cap">MISION</h2>
                            <p>Como empresa Distribuidora de Pisos, buscamos satisfacer las necesidades de nuestros clientes proporcionándoles productos de calidad, además de contar con la más completa gama para decorar los interiores de un hogar, oficina o comercio, ofreciendo un servicio óptimo a través de nuestro experto equipo de trabajo.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section About Intro -->
        <br>

        <footer class="footer-v1">
            <div class="footer-left">
                <a href="index">
                    <h4 style="color: white;"><span><strong>DISTRIBUIDORA DE PISOS</strong></span></h4>
                    <!--<img src="images\logotipo.gif" style="width: 150%;" class="img-responsive" alt="Image">-->
                </a>
            </div>
            <!-- End Left Footer -->
            <nav>
                <ul>
                    <li>
                        <a class="text-cap" href="index">INICIO</a>
                    </li>
                    <li>
                        <a class="text-cap" href="productos">PRODUCTOS</a>
                    </li>
                    <li>
                        <a class="text-cap" href="servicios">SERVICIOS</a>
                    </li>
                    <li>
                        <a class="text-cap" href="nosotros">¿Quienes somos?</a>
                    </li>
                    <li>
                        <a class="text-cap" href="contacto">contactanos</a>
                    </li>
                </ul>
            </nav>
            <!-- End Nav Footer -->
            <div class="footer-right">
                <ul class="social social-footer">
                    <li class="facebook active"><a><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a><i class="fa fa-google-plus"></i></a></li>
                    <li class="youtube"><a><i class="fa fa-youtube-play"></i></a></li>
                    <li class="linkedin"><a><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <!-- End Right Footer -->
        </footer>
        <!-- End Footer -->

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css\bootstrap.min.css">
        <!-- Font -->
        <link rel="stylesheet" href="css\font-awesome.min.css">
        <link rel="stylesheet" href="css\elegant-font.css">

        <!-- REVOLUTION STYLE SHEETS -->
        <link rel="stylesheet" type="text/css" href="revolution\css\settings.css">
        <!-- REVOLUTION LAYERS STYLES -->
        <link rel="stylesheet" type="text/css" href="revolution\css\layers.css">
        <!-- REVOLUTION NAVIGATION STYLES -->
        <link rel="stylesheet" type="text/css" href="revolution\css\navigation.css">
        <!-- OWL CAROUSEL
          ================================================== -->
        <link rel="stylesheet" href="css\owl.carousel.css">
        <!-- SCROLL BAR MOBILE MENU
        ================================================== -->
        <link rel="stylesheet" href="css\jquery.mCustomScrollbar.css">

        <!-- color scheme -->
        <link rel="stylesheet" href="switcher\demo.css" type="text/css">
        <link rel="stylesheet" href="switcher/colors/maroon.css" type="text/css" id="colors">

        <section class="copyright">
            <p>Copyright © 2020 Designed by <a href="esquitechs.com" style="color: deepskyblue;">Esquitechs</a>.
                All rights reserved.</p>
        </section>
    </div>
</div>
<!-- End page -->

<a id="to-the-top"><i class="fa fa-angle-up"></i></a>
<!-- Back To Top -->


<!-- SCRIPT -->
<script src="js\vendor\jquery.min.js"></script>
<script src="js\vendor\bootstrap.min.js"></script>
<script src="js\plugins\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js\plugins\wow.min.js"></script>
<script type="text/javascript" src="js\plugins\skrollr.min.js"></script>
<!-- Switcher
================================================== -->
<script src="switcher\demo.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="revolution\js\jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="revolution\js\jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
    (Load Extensions only on Local File Systems !
    The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.video.min.js"></script>
<!-- Intializing Slider-->
<script type="text/javascript" src="js\plugins\slider.js"></script>

<!-- Mobile Menu
================================================== -->
<script src="js\plugins\jquery.mobile-menu.js"></script>

<!-- Initializing the isotope
================================================== -->
<script src="js\plugins\isotope.pkgd.min.js"></script>
<script src="js\plugins\custom-isotope.js"></script>
<!-- Initializing Owl Carousel
================================================== -->
<script src="js\plugins\owl.carousel.js"></script>
<script src="js\plugins\custom-owl.js"></script>


<!-- PreLoad
================================================== -->
<!--<script type="text/javascript" src="js\plugins\royal_preloader.min.js"></script>
<script type="text/javascript">
    (function ($) {
        "use strict";
        Royal_Preloader.config({
            mode: 'logo',
            logo: 'images/logotipo.gif',
            timeout: 1,
            showInfo: false,
            opacity: 1,
            background: ['#FFFFFF']
        });
    })(jQuery);
</script>
-->
<!-- Global Js
================================================== -->
<script src="js\plugins\custom.js"></script>

</body>
</html>
