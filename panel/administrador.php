<?php

include '../conexion/conexion.php';

if (!isset($_SESSION['id']) || !isset($_SESSION['name']) || !isset($_SESSION['mail'])) {
    header('location: ../');
//cuando se ingresa del login se general las variables de sesion desde el login, aqui si existe se redirecciona a inicio
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Panel de administración</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<nav>
    <div class="nav-wrapper black">
        <a class="brand-logo center">Panel de control</a>
        <ul class="left hide-on-med-and-down">
            <li><a href="../login/Salir.php">Cerrar sesión</a></li>
        </ul>
    </div>
</nav>

<body>
<div class="container">
    <br><br>
    <div class="col s12 m6">
        <div class="card">
            <div class="card-content black-text">
                <span class="card-title centered">Promoción</span>
                <p>Por favor, ingrese la promoción para actualizarla</p>
            </div>
            <div class="card-action">
                <div class="row">
                    <div class="col s12">
                        <form action="promociones" method="post">
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="textarea1" name="promocion" class="materialize-textarea"></textarea>
                                    <label for="textarea1">Promoción</label>
                                </div>
                                <div class="input-field col s2">
                                    <button type="submit" class="waves-effect waves-light btn">Actualizar</button>
                                </div>
                            </div>
                        </form>
                        <form action="emptypromo.php" method="post">
                            <input type="hidden" name="promocion" value="">
                        <button type="submit" class="waves-effect waves-light btn" style="background-color: red;">Borrar promoción</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
</body>


<!--JavaScript at end of body for optimized loading-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

</html>