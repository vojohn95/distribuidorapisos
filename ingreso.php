<?php
@session_start();//incluyendo la conexion para sesion
include 'login/header.php';

?>

<body>
<div class="container">
    <br><br>
    <div class="col s12 m6">
        <div class="card">
            <div class="card-content black-text">
                <span class="card-title centered">INICIO DE SESIÓN</span>
                <p class="centered">Llene los campos para poder ingresar</p>
            </div>
            <div class="card-action">
                <div class="row">
                    <form class="col s12" action="login/index" method="post">
                        <div class="row">
                            <div class="input-field col s5">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="icon_prefix" type="email" name="mail_log" class="validate" required>
                                <label for="icon_prefix">Correo</label>
                            </div>
                            <div class="input-field col s5">
                                <i class="material-icons prefix">password</i>
                                <input id="icon_telephone"  class="validate" type="password" name="pass_log" required
                                       autocomplete="off">
                                <label for="icon_telephone">Contraseña</label>
                            </div>
                            <div class="input-field col s2">
                                <button type="submit" class="waves-effect waves-light btn">Entrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
</body>


<!--JavaScript at end of body for optimized loading-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

</html>
