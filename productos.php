<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Bienvenidos Distribuidora de Pisos. Somos una empresa 100% mexicana, dedicada a la decoración y ambientación de interiores con una amplia experiencia ...">
    <meta content="linoleums, alfombras, loseta vinílica, pisos laminados, y productos para diseño de interiores."/>
    <meta content="Venta especializada de pisos" property="og:title">
    <meta content="http://www.distribuidoradepisos.com.mx/" property="og:url">
    <meta content="Distribuidora de pisos" property="og:site_name">
    <meta content="website" property="og:type">
    <meta name="author" content="Esquitechs">
    <meta name="copyright" content="Esquitechs" />
    <meta property="og:locale" content="es_MX">
    <meta property="og:image" content="https:///distribuidoradepisos.com.mx/images/logotipo.gif">
    <meta property="og:description"
          content="Bienvenidos Distribuidora de Pisos. Somos una empresa 100% mexicana, dedicada a la decoración y ambientación de interiores con una amplia experiencia ...">
    <meta property="og:site_name" content="Distribuidora de pisos">
    <meta name="Keywords" content="pisos y azulejos, pisos, pisos cerámicos, pisos para bańos, azulejos para piso, azulejos, loseta, pisos, "/>
    <meta name="robots" content="index,follow"/>
    <meta http-equiv="expires" content="3600"/>

    <!-- SITE TITLE -->
    <title>Distribuidora de pisos | Empresa de desarrollo de software</title>

    <link rel=”alternate” hreflang=”es-MX” href=”http://distribuidoradepisos.com.mx/”/>
    <link rel=”canonical” href=”https://esquitechs.com/"/>
    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">


    <!-- Favicons
      ================================================== -->
    <link rel="shortcut icon" href="favicon.png">

    <style>@charset "UTF-8";@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);@import url(https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900&subset=latin,vietnamese);@-ms-viewport{width:auto!important}html{overflow-x:hidden}a{text-decoration:none}body{font-family:'Roboto',sans-serif;font-size:15px;line-height:25px;color:#666;overflow:hidden}td{vertical-align:middle!important}html,body{min-height:100%}@media only screen and (max-width:479px){body{font-size:14px}}</style>
</head>
<body>
<!--<div id="switcher">
    <span class="custom-close"></span>
   <span class="custom-show"></span>
    <div class="mCustomScrollbar" data-mcs-theme="minimal-dark">
           <a href="#" class="ot-btn btn-main-color block-btn">Buy Now</a>

           <div class="clearfix"></div>

           <span class="sw-title">Main Colors:</span>
           <ul id="tm-color">
               <li class="color1"></li>
               <li class="color2"></li>
               <li class="color3"></li>
               <li class="color4"></li>
               <li class="color5"></li>
               <li class="color6"></li>
               <li class="color7"></li>
           </ul>
           <div class="clearfix"></div>

           <span class="sw-title">Header Layout</span>
           <select name="switcher" id="de-header-layout">
               <option value="opt-1" selected="">Hide Top Bar</option>
               <option value="opt-2">Show Top Bar</option>
           </select>
           <div class="clearfix"></div>


           <span class="sw-title">Menu Seperator Style</span>
           <select name="switcher" id="de-menu">
               <option value="opt-1" selected="">Line Throu Separator</option>
               <option value="opt-2">Line Separator</option>
               <option value="opt-3">Circle Separator</option>
               <option value="opt-4">Square Separator</option>
               <option value="opt-5">Plus Separator</option>
               <option value="opt-6">Strip Separator</option>
               <option value="opt-0">No Separator</option>
           </select>

           <div class="clearfix"></div>

           <span class="sw-title">Hover Header Link Effect</span>
           <select name="switcher" id="de-menu-eff">
               <option value="opt-1" selected="">Line Through Effect</option>
               <option value="opt-2">Background Effect</option>
               <option value="opt-3">Text Color</option>
               <option value="opt-4">Line Expand</option>
           </select>

           <div class="clearfix"></div>


           <span class="sw-title">HomePage Layout</span>
           <ul class="demo-homepage">
               <li>
                   <a href="index">
                       <img src="switcher\images\home_1.webp" alt="">
                   </a>
                   <img src="switcher\images\home_1.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_2">
                       <img src="switcher\images\home_2.webp" alt="">
                   </a>
                   <img src="switcher\images\home_2.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_3">
                       <img src="switcher\images\home_3.webp" alt="">
                   </a>
                   <img src="switcher\images\home_3.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_4">
                       <img src="switcher\images\home_4.webp" alt="">
                   </a>
                   <img src="switcher\images\home_4.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_5">
                       <img src="switcher\images\home_5.webp" alt="">
                   </a>
                   <img src="switcher\images\home_5.webp" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_6">
                       <img src="switcher\images\home_6.webp" alt="">
                   </a>
                   <img src="switcher\images\home_6.webp" class="popup-demo-homepage" alt="Image">
               </li>
           </ul>
           </div>
</div>-->
<!-- End Switcher Color -->
<div class="mobile-menu-first">
    <div id="mobile-menu" class="mobile-menu">
        <div class="header-mobile-menu">
            <a class="text-cap hidden-xs">TEL: 56870562/ 56870623 / 55431941</a>
            <div class="mm-toggle">
                <span aria-hidden="true" class="icon_close"></span>
            </div>
        </div> <!-- Mobile Menu -->
        <div class="mCustomScrollbar light" data-mcs-theme="minimal-dark">

            <ul>
                <li class="has-sub"><a href="index"><span>Inicio</span></a>
                </li>
                <li class="has-sub"><a href="productos"><span>Productos</span></a>
                </li>
                <li><a href="servicios"><span>Servicios</span></a></li>
                <li><a href="nosotros"><span>¿Quienes somos? </span></a></li>

                <!--<li><a href="blogList"><span>Blog</span></a>
                    <ul>
                        <li><a href="blogGrid_2_col">Grid 2 Columns</a></li>
                        <li><a href="blogGrid_3_col">Grid 3 Columns</a></li>
                        <li><a href="blogGrid_4_col">Grid 4 Columns</a></li>
                        <li><a href="blogList">Blog List</a></li>
                        <li><a href="blogDetail">Blog Detail</a></li>
                    </ul>
                </li>
                <li><a href=""><span>Pages</span></a>
                    <ul>
                        <li><a href="elements">Element</a></li>
                        <li><a href="typography">Typography</a></li>
                        <li><a href="404">404 Page</a></li>
                        <li><a href="commingsoon">Comming Soon</a></li>
                    </ul>
                </li>
                <li><a href="shop_catalog"><span>Shop</span></a>
                    <ul>
                        <li><a href="shop_catalog">Store Catalog</a></li>
                        <li><a href="shop_cart">Shopping Cart</a></li>

                        <li><a href="shop_single">Single Product</a></li>
                    </ul>
                </li>-->
                <li><a href="contacto"><span>contacto</span></a></li>
            </ul>
        </div> <!-- /#rmm   -->
    </div>
</div><!-- End Mobile Menu -->

<div class="modal fade modal-search" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <div class="modal-dialog myModal-search">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <form role="search" method="get" class="search-form">
                    <input class="search-field" placeholder="Search here..." value="" title="" type="search">
                    <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Search-->
<div id="page">
    <div id="skrollr-body">
        <header id="mainmenu" class="header-v1 header-border header-fix header-bg-white"
                data-0="padding:10px;padding-left:40px;padding-right:40px;"
                data-251="padding:10px; padding-left:40px;padding-right:40px;top:0;">
            <div id="info" class="topbar topbar-position topbar-dark hide-topbar" data-0="height:30px"
                 data-251="height:0;">

                <div class="col-md-12">
                    <p class="text-cap hidden-xs">TEL: 56870562/ 56870623 / 55431941</p>
                    <p class="text-cap">distridepisos@yahoo.com.mx</p>
                    <!--<div class="language">
                        <a href="#" class="active">EN</a>
                        <a href="#">FR</a>
                        <a href="#">PT</a>
                    </div>-->
                </div>

            </div>
            <div class="left-header">
                <ul class="navi-level-1">
                    <li><a href="index" class="logo"><img src="images\logotipo.gif" style="width: 150%;"
                                                               class="img-responsive" alt="distribuidora de pisos"></a>
                    </li>
                </ul>
            </div><!-- End Left Header -->
            <nav>
                <ul class="navi-level-1 hover-style-2 main-navi">
                    <li class="has-sub"><a href="index"><span>Inicio</span></a>
                    </li>
                    <li class="has-sub"><a href="productos"><span>Productos</span></a>
                    </li>
                    <li><a href="servicios"><span>Servicios</span></a></li>
                    <li><a href="nosotros"><span>¿Quienes somos? </span></a></li>


                    <!--<li class="has-sub"><a href="blogList"><span>Blog</span></a>
                        <ul class="navi-level-2">
                            <li><a href="blogGrid_2_col">Grid 2 Columns</a></li>
                            <li><a href="blogGrid_3_col">Grid 3 Columns</a></li>
                            <li><a href="blogGrid_4_col">Grid 4 Columns</a></li>
                            <li><a href="blogList">Blog List</a></li>
                            <li><a href="blogDetail">Blog Detail</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"><a href=""><span>Pages</span></a>
                        <ul class="navi-level-2">
                            <li><a href="elements">Element</a></li>
                            <li><a href="typography">Typography</a></li>
                            <li><a href="404">404 Page</a></li>
                            <li><a href="commingsoon">Comming Soon</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"><a href="shop_catalog"><span>Shop</span></a>
                        <ul class="navi-level-2">
                            <li><a href="shop_catalog">Store Catalog</a></li>
                            <li><a href="shop_cart">Shopping Cart</a></li>

                            <li><a href="shop_single">Single Product</a></li>
                        </ul>
                    </li>-->
                    <li class="has-sub"><a href="contacto"><span>contactanos</span></a></li>

                </ul>
            </nav><!-- End Nav -->
            <div class="right-header">
                <ul class="navi-level-1 sub-navi seperator-horizonal-line hover-style-4">
                    <li class="header-tel"><a class="tel-header">TEL: 56870562/ 56870623 / 55431941</a></li>


                    <!-- Testing Search Box -->
                    <!--<li><a href="#"><span aria-hidden="true" class="icon_bag_alt"></span>
                        </a>
                    </li>
                     <li>
                        <a href="#" data-toggle="modal" data-target="#myModal" id="btn-search" class="reset-btn btn-in-navi"><span aria-hidden="true" class="icon_search"></span></a>
                    </li>-->
                    <li>
                        <a href="#/" class="mm-toggle">
                            <span aria-hidden="true" class="icon_menu"></span>
                        </a>
                    </li>
                </ul>

            </div><!-- End Right Header -->
            <!-- End Right Header -->
        </header>
        <!-- End  Header -->
        <section>
            <div class="sub-header sub-header-1 sub-header-portfolio-grid-1 fake-position">
                <div class="sub-header-content">
                    <h2 class="text-cap white-text">Productos</h2>
                    <ol class="breadcrumb breadcrumb-arc text-cap">
                        <li>
                            <a href="index">INICIO</a>
                        </li>
                        <li class="active">Productos</li>
                    </ol>
                </div>
            </div>
        </section>
        <!-- End Section Sub Header -->
        <section class="padding bg-grey padding-bottom-0">
            <div class="lastest-project-warp portfolio-grid-2-warp clearfix">
                <div class="projectFilter project-terms line-effect-2">
                    <a href="#" data-filter="*" class="current text-cap">
                        <h4>TODOS</h4>
                    </a>
                    <a href="#" data-filter=".Residential" class="text-cap">
                        <h4>Persianas</h4>
                    </a>
                    <a href="#" data-filter=".Ecommercial" class="text-cap">
                        <h4>MODELOS</h4>
                    </a>
                    <a href="#" data-filter=".Office" class="text-cap">
                        <h4>GEOMETRÍAS</h4>
                    </a>
                    <a href="#" data-filter=".Hospital" class="text-cap">
                        <h4>PISOS</h4>
                    </a>
                    <a href="#" data-filter=".alfombras" class="text-cap">
                        <h4>Alfombras</h4>
                    </a>
                    <a href="#" data-filter=".Tapíz" class="text-cap">
                        <h4>Tapíz</h4>
                    </a>
                </div>
                <!-- End Project Fillter -->
                <div class="clearfix projectContainer portfolio-grid-2-container">
                    <div class="element-item  Residential">
                        <a class="portfolio-img-demo"><img src="images\Project\1.webp"
                                                                                         class="img-responsive"
                                                                                         alt="Image"></a>
                        <div class="project-info">
                            <a class="text-cap">
                                <h4 class="title-project text-cap text-cap">PERSIANAS</h4>
                                <ul align="justify">
                                    <li>PVC</li>
                                    <li>TELA</li>
                                    <li>MADERA</li>
                                    <li>ALUMINIO</li>
                                    <li>PANEL JAPONÉS</li>
                                    <li>CELULARES</li>
                                    <li>Y MÁS</li>
                                </ul>
                            </a>

                        </div>
                    </div>
                    <div class="element-item Ecommercial ">
                        <a class="portfolio-img-demo"><img src="images\Project\2.webp"
                                                                                         class="img-responsive"
                                                                                         alt="Image"></a>
                        <div class="project-info">
                            <a class="text-cap">
                                <h4 class="title-project text-cap">MODELOS</h4>
                                <ul align="justify">
                                    <li>Enrollables</li>
                                    <li>Plisadas</li>
                                    <li>Romanas</li>
                                    <li>Noche y día</li>
                                    <li>PANEL JAPONÉS</li>
                                    <li>CELULARES</li>
                                    <li>Motorizadas</li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element-item Office">
                        <a class="portfolio-img-demo"><img src="images\Project\3.webp"
                                                                                         class="img-responsive"
                                                                                         alt="Image"></a>
                        <div class="project-info">
                            <a class="text-cap">
                                <h4 class="title-project text-cap">GEOMETRÍAS</h4>
                                <ul align="justify">
                                    <li>verticales</li>
                                    <li>horizontales</li>
                                    <li>domos</li>
                                    <li>formas especiales</li>
                                    <li style="visibility: hidden;"></li>
                                    <li style="visibility: hidden;"></li>
                                    <li style="visibility: hidden;"></li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element-item Hospital ">
                        <a class="portfolio-img-demo"><img src="images\Project\4.webp"
                                                                                         class="img-responsive"
                                                                                         alt="Image"></a>
                        <div class="project-info">
                            <a class="text-cap">
                                <h4 class="title-project text-cap">Pisos</h4>
                                <ul align="justify">
                                    <li>Loseta y duela vinílica</li>
                                    <li>Vinílico en rollo</li>
                                    <li>Duela laminada</li>
                                    <li>formas especiales</li>
                                    <li>Deck (para interiores y exteriores)</li>
                                    <li>Corcho</li>
                                    <li style="visibility: hidden;"></li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element-item alfombras">
                        <a class="portfolio-img-demo" href="portfoloDetail_1"><img src="images\Project\5.webp"
                                                                                         class="img-responsive"
                                                                                         alt="Image"></a>
                        <div class="project-info">
                            <a class="text-cap">
                                <h4 class="title-project text-cap">ALFOMBRAS</h4>
                                <ul align="justify">
                                    <li>Nacionales e importadas</li>
                                    <li>Residenciales</li>
                                    <li>Para tráfico pesado</li>
                                    <li>Institucionales</li>
                                    <li>Pasto sintético</li>
                                    <li style="visibility: hidden;"></li>
                                    <li style="visibility: hidden;"></li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element-item Tapíz">
                        <a class="portfolio-img-demo"><img src="images\Project\6.webp"
                                                                                         class="img-responsive"
                                                                                         alt="Image"></a>
                        <div class="project-info">
                            <a class="text-cap">
                                <h4 class="title-project text-cap">TAPÍZ</h4>
                                <ul align="justify">
                                    <li>tela</li>
                                    <li>Plástico</li>
                                    <li>Papel</li>
                                    <li>Corcho</li>
                                    <li style="visibility: hidden;">Pasto sintético</li>
                                    <li style="visibility: hidden;"></li>
                                    <li style="visibility: hidden;"></li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <!-- mas contenedores -->
                    <!--
                                         <div class="element-item Hospital ">
                                            <a class="portfolio-img-demo" href="portfolioDetail_1"><img src="images\Project\7.webp" class="img-responsive" alt="Image"></a>
                                            <div class="project-info">
                                               <a href="portfolioDetail">
                                                  <h4 class="title-project text-cap">Ogrange Corporate</h4>
                                               </a>
                                               <a href="portfolioDetail" class="cateProject">Office</a>
                                            </div>
                                         </div>
                                         <div class="element-item Hospital">
                                            <a class="portfolio-img-demo" href="portfolioDetail_1"><img src="images\Project\8.webp" class="img-responsive" alt="Image"></a>
                                            <div class="project-info">
                                               <a href="portfolioDetail">
                                                  <h4 class="title-project text-cap">Ocean view Building</h4>
                                               </a>
                                               <a href="portfolioDetail" class="cateProject">Residential</a>
                                            </div>
                                         </div>
                                         <div class="element-item  Residential">
                                            <a class="portfolio-img-demo" href="portfolioDetail_1"><img src="images\Project\9.webp" class="img-responsive" alt="Image"></a>
                                            <div class="project-info">
                                               <a href="portfolioDetail">
                                                  <h4 class="title-project text-cap text-cap">Modern Design</h4>
                                               </a>
                                               <a href="portfolioDetail" class="cateProject">Resident</a>
                                            </div>
                                         </div>
                                         <div class="element-item Residential ">
                                            <a class="portfolio-img-demo" href="portfolioDetail_1"><img src="images\Project\10.webp" class="img-responsive" alt="Image"></a>
                                            <div class="project-info">
                                               <a href="portfolioDetail">
                                                  <h4 class="title-project text-cap">Dream house</h4>
                                               </a>
                                               <a href="portfolioDetail" class="cateProject">Residential</a>
                                            </div>
                                         </div>
                                         <div class="element-item Ecommercial">
                                            <a class="portfolio-img-demo" href="portfolioDetail_1"><img src="images\Project\12.webp" class="img-responsive" alt="Image"></a>
                                            <div class="project-info">
                                               <a href="portfolioDetail">
                                                  <h4 class="title-project text-cap">Living Room Art</h4>
                                               </a>
                                               <a href="portfolioDetail" class="cateProject">Residential</a>
                                            </div>
                                         </div>
                                         <div class="element-item Ecommercial ">
                                            <a class="portfolio-img-demo" href="portfolioDetail_1"><img src="images\Project\12.webp" class="img-responsive" alt="Image"></a>
                                            <div class="project-info">
                                               <a href="portfolioDetail">
                                                  <h4 class="title-project text-cap">Bedroom Design Awward</h4>
                                               </a>
                                               <a href="portfolioDetail" class="cateProject">Residential</a>
                                            </div>
                                         </div>
                                      </div>
                                      -->
                    <!-- End project Container -->
                </div>
                <!-- End  -->
                <div class="overlay-arc">
                    <div class="layer-1">
                        <a href="contacto" class="ot-btn btn-border btn-border-dark btn-long text-cap">contacto</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section Isotop Lastest Project -->
        <footer class="footer-v1">
            <div class="footer-left">
                <a href="index">
                    <h4 style="color: white;"><span><strong>DISTRIBUIDORA DE PISOS</strong></span></h4>
                    <!--<img src="images\logotipo.gif" style="width: 150%;" class="img-responsive" alt="Image">-->
                </a>
            </div>
            <!-- End Left Footer -->
            <nav>
                <ul>
                    <li>
                        <a class="text-cap" href="index">INICIO</a>
                    </li>
                    <li>
                        <a class="text-cap" href="productos">PRODUCTOS</a>
                    </li>
                    <li>
                        <a class="text-cap" href="servicios">SERVICIOS</a>
                    </li>
                    <li>
                        <a class="text-cap" href="nosotros">¿Quienes somos?</a>
                    </li>
                    <li>
                        <a class="text-cap" href="contacto">contactanos</a>
                    </li>
                </ul>
            </nav>
            <!-- End Nav Footer -->
            <div class="footer-right">
                <ul class="social social-footer">
                    <li class="facebook active"><a><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a><i class="fa fa-google-plus"></i></a></li>
                    <li class="youtube"><a><i class="fa fa-youtube-play"></i></a></li>
                    <li class="linkedin"><a><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <!-- End Right Footer -->
        </footer>
        <!-- End Footer -->

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css\bootstrap.min.css">
        <!-- Font -->
        <link rel="stylesheet" href="css\font-awesome.min.css">
        <link rel="stylesheet" href="css\elegant-font.css">

        <!-- REVOLUTION STYLE SHEETS -->
        <link rel="stylesheet" type="text/css" href="revolution\css\settings.css">
        <!-- REVOLUTION LAYERS STYLES -->
        <link rel="stylesheet" type="text/css" href="revolution\css\layers.css">
        <!-- REVOLUTION NAVIGATION STYLES -->
        <link rel="stylesheet" type="text/css" href="revolution\css\navigation.css">
        <!-- OWL CAROUSEL
          ================================================== -->
        <link rel="stylesheet" href="css\owl.carousel.css">
        <!-- SCROLL BAR MOBILE MENU
        ================================================== -->
        <link rel="stylesheet" href="css\jquery.mCustomScrollbar.css">

        <!-- color scheme -->
        <link rel="stylesheet" href="switcher\demo.css" type="text/css">
        <link rel="stylesheet" href="switcher/colors/maroon.css" type="text/css" id="colors">

        <section class="copyright">
            <p>Copyright © 2020 Designed by <a href="esquitechs.com" style="color: deepskyblue;">Esquitechs</a>.
                All rights reserved.</p>
        </section>
    </div>
</div>
<!-- End page -->

<a id="to-the-top"><i class="fa fa-angle-up"></i></a>
<!-- Back To Top -->


<!-- SCRIPT -->
<script src="js\vendor\jquery.min.js"></script>
<script src="js\vendor\bootstrap.min.js"></script>
<script src="js\plugins\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js\plugins\wow.min.js"></script>
<script type="text/javascript" src="js\plugins\skrollr.min.js"></script>
<!-- Switcher
================================================== -->
<script src="switcher\demo.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="revolution\js\jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="revolution\js\jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
    (Load Extensions only on Local File Systems !
    The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.video.min.js"></script>
<!-- Intializing Slider-->
<script type="text/javascript" src="js\plugins\slider.js"></script>

<!-- Mobile Menu
================================================== -->
<script src="js\plugins\jquery.mobile-menu.js"></script>

<!-- Initializing the isotope
================================================== -->
<script src="js\plugins\isotope.pkgd.min.js"></script>
<script src="js\plugins\custom-isotope.js"></script>
<!-- Initializing Owl Carousel
================================================== -->
<script src="js\plugins\owl.carousel.js"></script>
<script src="js\plugins\custom-owl.js"></script>


<!-- PreLoad
================================================== -->
<!--<script type="text/javascript" src="js\plugins\royal_preloader.min.js"></script>
<script type="text/javascript">
    (function ($) {
        "use strict";
        Royal_Preloader.config({
            mode: 'logo',
            logo: 'images/logotipo.gif',
            timeout: 1,
            showInfo: false,
            opacity: 1,
            background: ['#FFFFFF']
        });
    })(jQuery);
</script>
-->
<!-- Global Js
================================================== -->
<script src="js\plugins\custom.js"></script>

</body>
</html>
