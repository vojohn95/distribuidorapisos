<?php

@session_start();//incluyendo la conexion para sesion?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Bienvenidos Distribuidora de Pisos. Somos una empresa 100% mexicana, dedicada a la decoración y ambientación de interiores con una amplia experiencia ...">
    <meta content="linoleums, alfombras, loseta vinílica, pisos laminados, y productos para diseño de interiores."/>
    <meta content="Venta especializada de pisos" property="og:title">
    <meta content="http://www.distribuidoradepisos.com.mx/" property="og:url">
    <meta content="Distribuidora de pisos" property="og:site_name">
    <meta content="website" property="og:type">
    <meta name="author" content="Esquitechs">
    <meta name="copyright" content="Esquitechs" />
    <meta property="og:locale" content="es_MX">
    <meta property="og:image" content="https:///distribuidoradepisos.com.mx/images/logotipo.gif">
    <meta property="og:description"
          content="Bienvenidos Distribuidora de Pisos. Somos una empresa 100% mexicana, dedicada a la decoración y ambientación de interiores con una amplia experiencia ...">
    <meta property="og:site_name" content="Distribuidora de pisos">
    <meta name="Keywords" content="pisos y azulejos, pisos, pisos cerámicos, pisos para bańos, azulejos para piso, azulejos, loseta, pisos, "/>
    <meta name="robots" content="index,follow"/>
    <meta http-equiv="expires" content="3600"/>

    <!-- SITE TITLE -->
    <title>Distribuidora de pisos | Empresa de desarrollo de software</title>

    <link rel=”alternate” hreflang=”es-MX” href=”http://distribuidoradepisos.com.mx/”/>
    <link rel=”canonical” href=”https://esquitechs.com/"/>
    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">


    <!-- Favicons
      ================================================== -->
    <link rel="shortcut icon" href="favicon.png">

    <style>
        @charset "UTF-8";@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);@import url(https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900&subset=latin,vietnamese);@-ms-viewport{width:auto!important}html{overflow-x:hidden}a{text-decoration:none}body{font-family:'Roboto',sans-serif;font-size:15px;line-height:25px;color:#666;overflow:hidden}td{vertical-align:middle!important}html,body{min-height:100%}@media only screen and (max-width:479px){body{font-size:14px}}
    </style>
</head>


<body>
<!--<div id="switcher">
    <span class="custom-close"></span>
   <span class="custom-show"></span>
    <div class="mCustomScrollbar" data-mcs-theme="minimal-dark">
           <a href="#" class="ot-btn btn-main-color block-btn">Buy Now</a>

           <div class="clearfix"></div>

           <span class="sw-title">Main Colors:</span>
           <ul id="tm-color">
               <li class="color1"></li>
               <li class="color2"></li>
               <li class="color3"></li>
               <li class="color4"></li>
               <li class="color5"></li>
               <li class="color6"></li>
               <li class="color7"></li>
           </ul>
           <div class="clearfix"></div>

           <span class="sw-title">Header Layout</span>
           <select name="switcher" id="de-header-layout">
               <option value="opt-1" selected="">Hide Top Bar</option>
               <option value="opt-2">Show Top Bar</option>
           </select>
           <div class="clearfix"></div>


           <span class="sw-title">Menu Seperator Style</span>
           <select name="switcher" id="de-menu">
               <option value="opt-1" selected="">Line Throu Separator</option>
               <option value="opt-2">Line Separator</option>
               <option value="opt-3">Circle Separator</option>
               <option value="opt-4">Square Separator</option>
               <option value="opt-5">Plus Separator</option>
               <option value="opt-6">Strip Separator</option>
               <option value="opt-0">No Separator</option>
           </select>

           <div class="clearfix"></div>

           <span class="sw-title">Hover Header Link Effect</span>
           <select name="switcher" id="de-menu-eff">
               <option value="opt-1" selected="">Line Through Effect</option>
               <option value="opt-2">Background Effect</option>
               <option value="opt-3">Text Color</option>
               <option value="opt-4">Line Expand</option>
           </select>

           <div class="clearfix"></div>


           <span class="sw-title">HomePage Layout</span>
           <ul class="demo-homepage">
               <li>
                   <a href="index">
                       <img src="switcher\images\home_1.jpg" alt="">
                   </a>
                   <img src="switcher\images\home_1.jpg" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_2">
                       <img src="switcher\images\home_2.jpg" alt="">
                   </a>
                   <img src="switcher\images\home_2.jpg" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_3">
                       <img src="switcher\images\home_3.jpg" alt="">
                   </a>
                   <img src="switcher\images\home_3.jpg" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_4">
                       <img src="switcher\images\home_4.jpg" alt="">
                   </a>
                   <img src="switcher\images\home_4.jpg" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_5">
                       <img src="switcher\images\home_5.jpg" alt="">
                   </a>
                   <img src="switcher\images\home_5.jpg" class="popup-demo-homepage" alt="Image">
               </li>
               <li>
                   <a href="home_6">
                       <img src="switcher\images\home_6.jpg" alt="">
                   </a>
                   <img src="switcher\images\home_6.jpg" class="popup-demo-homepage" alt="Image">
               </li>
           </ul>
           </div>
</div>-->
<!-- End Switcher Color -->
<div class="mobile-menu-first">
    <div id="mobile-menu" class="mobile-menu">
        <div class="header-mobile-menu">
            <a class="text-cap hidden-xs">TEL:(55) (55)56870562/(55) (55)56870623 /(55) (55)55431941</a>
            <div class="mm-toggle">
                <span aria-hidden="true" class="icon_close"></span>
            </div>
        </div> <!-- Mobile Menu -->
        <div class="mCustomScrollbar light" data-mcs-theme="minimal-dark">

            <ul>
                <li class="has-sub"><a href="index"><span>Inicio</span></a>
                </li>
                <li class="has-sub"><a href="productos"><span>Productos</span></a>
                </li>
                <li><a href="servicios"><span>Servicios</span></a></li>
                <li><a href="nosotros"><span>¿Quienes somos? </span></a></li>

                <!--<li><a href="blogList"><span>Blog</span></a>
                    <ul>
                        <li><a href="blogGrid_2_col">Grid 2 Columns</a></li>
                        <li><a href="blogGrid_3_col">Grid 3 Columns</a></li>
                        <li><a href="blogGrid_4_col">Grid 4 Columns</a></li>
                        <li><a href="blogList">Blog List</a></li>
                        <li><a href="blogDetail">Blog Detail</a></li>
                    </ul>
                </li>
                <li><a href=""><span>Pages</span></a>
                    <ul>
                        <li><a href="elements">Element</a></li>
                        <li><a href="typography">Typography</a></li>
                        <li><a href="404">404 Page</a></li>
                        <li><a href="commingsoon">Comming Soon</a></li>
                    </ul>
                </li>
                <li><a href="shop_catalog"><span>Shop</span></a>
                    <ul>
                        <li><a href="shop_catalog">Store Catalog</a></li>
                        <li><a href="shop_cart">Shopping Cart</a></li>

                        <li><a href="shop_single">Single Product</a></li>
                    </ul>
                </li>-->
                <li><a href="contacto"><span>contacto</span></a></li>
            </ul>
        </div> <!-- /#rmm   -->
    </div>
</div><!-- End Mobile Menu -->

<div class="modal fade modal-search" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <div class="modal-dialog myModal-search">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <form role="search" method="get" class="search-form">
                    <input class="search-field" placeholder="Search here..." value="" title="" type="search">
                    <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Search-->
<div id="page">
    <div id="skrollr-body">
        <header id="mainmenu" class="header-v1 header-border header-fix header-bg-white"
                data-0="padding:10px;padding-left:40px;padding-right:40px;"
                data-251="padding:10px; padding-left:40px;padding-right:40px;top:0;">
            <div id="info" class="topbar topbar-position topbar-dark hide-topbar" data-0="height:30px"
                 data-251="height:0;">

                <div class="col-md-12">
                    <p class="text-cap hidden-xs">TEL:(55) 56870562/(55) 56870623 /(55) 55431941</p>
                    <p class="text-cap">distridepisos@yahoo.com.mx</p>
                    <!--<div class="language">
                        <a href="#" class="active">EN</a>
                        <a href="#">FR</a>
                        <a href="#">PT</a>
                    </div>-->
                </div>

            </div>
            <div class="left-header">
                <ul class="navi-level-1">
                    <li><a href="index" class="logo"><img src="images\logotipo.gif" style="width: 150%;"
                                                               class="img-responsive" alt="distribuidora de pisos"></a>
                    </li>
                </ul>
            </div><!-- End Left Header -->
            <nav>
                <ul class="navi-level-1 hover-style-2 main-navi">
                    <li class="has-sub"><a href="index"><span>Inicio</span></a>
                    </li>
                    <li class="has-sub"><a href="productos"><span>Productos</span></a>
                    </li>
                    <li><a href="servicios"><span>Servicios</span></a></li>
                    <li><a href="nosotros"><span>¿Quienes somos? </span></a></li>


                    <!--<li class="has-sub"><a href="blogList"><span>Blog</span></a>
                        <ul class="navi-level-2">
                            <li><a href="blogGrid_2_col">Grid 2 Columns</a></li>
                            <li><a href="blogGrid_3_col">Grid 3 Columns</a></li>
                            <li><a href="blogGrid_4_col">Grid 4 Columns</a></li>
                            <li><a href="blogList">Blog List</a></li>
                            <li><a href="blogDetail">Blog Detail</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"><a href=""><span>Pages</span></a>
                        <ul class="navi-level-2">
                            <li><a href="elements">Element</a></li>
                            <li><a href="typography">Typography</a></li>
                            <li><a href="404">404 Page</a></li>
                            <li><a href="commingsoon">Comming Soon</a></li>
                        </ul>
                    </li>
                    <li class="has-sub"><a href="shop_catalog"><span>Shop</span></a>
                        <ul class="navi-level-2">
                            <li><a href="shop_catalog">Store Catalog</a></li>
                            <li><a href="shop_cart">Shopping Cart</a></li>

                            <li><a href="shop_single">Single Product</a></li>
                        </ul>
                    </li>-->
                    <li class="has-sub"><a href="contacto"><span>contactanos</span></a></li>

                </ul>
            </nav><!-- End Nav -->
            <div class="right-header">
                <ul class="navi-level-1 sub-navi seperator-horizonal-line hover-style-4">
                    <li class="header-tel"><a class="tel-header">TEL:(55) 56870562/(55) 56870623 /(55) 55431941</a></li>


                    <!-- Testing Search Box -->
                    <!--<li><a href="#"><span aria-hidden="true" class="icon_bag_alt"></span>
                        </a>
                    </li>
                     <li>
                        <a href="#" data-toggle="modal" data-target="#myModal" id="btn-search" class="reset-btn btn-in-navi"><span aria-hidden="true" class="icon_search"></span></a>
                    </li>-->
                    <li>
                        <a href="#/" class="mm-toggle">
                            <span aria-hidden="true" class="icon_menu"></span>
                        </a>
                    </li>
                </ul>

            </div><!-- End Right Header -->
        </header>
        <!-- End  Header -->

        <section>
            <div class="rev_slider_wrapper">
                <!-- START REVOLUTION SLIDER 5.0 auto mode -->
                <div id="slider-h2" class="rev_slider slider-home-2" data-version="5.0">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-masterspeed="10">

                            <!-- MAIN IMAGE -->
                            <img src="images\Slider\h2-1.webp" alt="" data-bgposition="center center" data-kenburns="on"
                                 data-duration="20000" data-ease="Linear.easeNone" data-scalestart="120"
                                 data-scaleend="140" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                                 data-no-retina="">

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption heading-1 white-text text-cap " data-x="center" data-y="center"
                                 data-voffset="-80" data-transform_in="y:-50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:-50px;opacity:0;s:300;" data-start="800">Distribuidora de pisos
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption heading-2 white-text" data-x="center" data-y="center"
                                 data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1400">Superamos la imaginación
                                de cualquier decorador
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption btn-1" data-x="center" data-hoffset="-85" data-y="center"
                                 data-voffset="80" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1600">
                                <a href="productos" class="ot-btn btn-main-color text-cap ">Productos</a>

                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption btn-2" data-x="center" data-hoffset="85" data-y="center"
                                 data-voffset="80" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1600">
                                <a href="contacto" class="ot-btn btn-sub-color text-cap  ">contacto</a>
                            </div>


                        </li>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-masterspeed="1000">

                            <!-- MAIN IMAGE -->
                            <img src="images\Slider\h3-1.webp" alt="Pisos" data-bgposition="center center" data-kenburns="on"
                                 data-duration="20000" data-ease="Linear.easeNone" data-scalestart="120"
                                 data-scaleend="140" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                                 data-no-retina="">

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption heading-1 white-text text-cap " data-x="center" data-y="center"
                                 data-voffset="-80" data-transform_idle="o:1;"
                                 data-transform_in="y:-50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:-50px;opacity:0;s:300;" data-start="800">Distribuidora de pisos
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption heading-2 white-text" data-x="center" data-y="center"
                                 data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1400">Elige tu estilo
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption btn-1" data-x="center" data-hoffset="-85" data-y="center"
                                 data-voffset="80" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1600">
                                <a href="productos" class="ot-btn btn-main-color text-cap ">Productos</a>

                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption btn-2" data-x="center" data-hoffset="85" data-y="center"
                                 data-voffset="80" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1600">
                                <a href="contacto" class="ot-btn btn-sub-color text-cap  ">contacto</a>
                            </div>


                        </li>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-masterspeed="1000">

                            <!-- MAIN IMAGE -->
                            <img src="images\Slider\fondo3.webp" alt="distribuidora de pisos" data-bgposition="center center" data-kenburns="on"
                                 data-duration="20000" data-ease="Linear.easeNone" data-scalestart="130"
                                 data-scaleend="150" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                                 data-no-retina="">

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption heading-1 white-text text-cap " data-x="center" data-y="center"
                                 data-voffset="-80" data-transform_idle="o:1;"
                                 data-transform_in="y:-50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:-50px;opacity:0;s:300;" data-start="800">Distribuidora de pisos
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption heading-2 white-text" data-x="center" data-y="center"
                                 data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1400">Pequeño cambio, gran
                                diferencia
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption btn-1" data-x="center" data-hoffset="-85" data-y="center"
                                 data-voffset="80" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1600">
                                <a href="productos" class="ot-btn btn-main-color text-cap ">Productos</a>

                            </div>s
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption btn-2" data-x="center" data-hoffset="85" data-y="center"
                                 data-voffset="80" data-transform_in="y:50px;opacity:0;s:800;e:easeInOutCubic;"
                                 data-transform_out="y:50px;opacity:0;s:300;" data-start="1600">
                                <a href="contacto" class="ot-btn btn-sub-color text-cap  ">contacto</a>
                            </div>


                        </li>
                    </ul>
                </div><!-- END REVOLUTION SLIDER -->
            </div><!-- END REVOLUTION SLIDER WRAPPER -->
        </section>
        <!-- End Section Slider -->

        <section class="padding">
            <div class="container">
                <div class="row">
                    <div class="title-block">
                        <h3 class="title text-cap">Somos una empresa 100% mexicana, dedicada a la decoración y
                            ambientación de
                            interiores con una amplia experiencia que nos avala.</h3>

                        <div class="divider divider-1">
                            <svg class="svg-triangle-icon-container">
                                <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                            </svg>
                        </div>
                    </div>
                    <!-- End Title -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="block-img-right">

                                <div class="img-block"><img src="images\Services\1.webp" class="img-responsive"
                                                            alt="Asesoria en decoración"></div>
                                <div class="text-box">
                                    <h4 class="text-cap">
                                        <mark>Asesoria</mark>
                                        en decoración
                                    </h4>
                                    <p>
                                        Personal especializado en decoración del hogar para asesorarte.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block-img-left">
                                <div class="img-block"><img src="images\Services\2.webp" class="img-responsive"
                                                            alt="Ribetes"></div>
                                <div class="text-box">
                                    <h4 class="text-cap">
                                        <mark>Ri</mark>
                                        betes
                                    </h4>
                                    <p>
                                        Diseños modernos para tus tapetes.
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block-img-right mgb0">
                                <div class="img-block"><img src="images\Services\3.webp" class="img-responsive"
                                                            alt="Alfombras y tapetes"></div>
                                <div class="text-box">
                                    <h4 class="text-cap">
                                        <mark>Diseño en</mark>
                                        alfombras y tapetes
                                    </h4>
                                    <p>
                                        Tapetes vanguardistas y los mejores diseños para todos los estilos.
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block-img-left mgb0">
                                <div class="img-block"><img src="images\Services\4.webp" class="img-responsive"
                                                            alt="Image"></div>
                                <div class="text-box">
                                    <h4 class="text-cap">
                                        <mark>Antiderrapantes</mark>
                                        para pisos y tapetes
                                    </h4>
                                    <p>
                                        Pisos y tapetes antiderrapantes para todos los espacios, además la mejor calidad
                                        de materiales y diseños.
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section What we do -->

        <section>
            <div class="promotion-box">
                <figure class="effect-layla">
                    <img src="images\Promotion\1.webp" alt="img06">
                    <figcaption>
                        <a class="ot-btn btn-main-color text-cap">Estilo</a>
                        <h3 class="text-cap white-text">Elegancia y vanguardia de nuestras
                            Duelas laminadas </h3>
                    </figcaption>
                </figure>
                <figure class="effect-layla">
                    <img src="images\Promotion\2.webp" alt="img06">
                    <figcaption>
                        <a class="ot-btn btn-main-color text-cap">Colorido</a>
                        <h3 class="text-cap white-text">Conoce nuestro catálogo de
                            Alfombras </h3>
                    </figcaption>
                </figure>
                <figure class="effect-layla">
                    <img src="images\Promotion\3.webp" alt="img06">
                    <figcaption>
                        <a class="ot-btn btn-main-color text-cap">Modernidad</a>
                        <h3 class="text-cap white-text">Persianas para todos los estilos.</h3>
                    </figcaption>
                </figure>
            </div>
        </section>
        <!-- End Section Promotion -->

        <section class="padding clearfix fixbug-inline-block ">
            <div class="container">
                <div class="row">
                    <div class="title-block">
                        <div class="title-block">
                            <h2 class="title text-cap">¿Porqué nosotros?</h2>
                            <div class="divider divider-1">
                                <svg class="svg-triangle-icon-container">
                                    <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <!-- End Title -->
                    <div class="chooseus-container text-center">
                        <div class="chooseus-item">
                            <h4 class="text-cap">Creatividad</h4>
                            <div class="chooseus-canvas-item">
                                <svg class="svg-hexagon">
                                    <polygon class="hexagon"
                                             points="285 100,285 250,155 325,25 250,25 100,155 25"></polygon>
                                </svg>
                                <!-- End Hexagon -->
                                <svg class="svg-triangle-dotted">
                                    <polygon class="triangle-div" points="2 220,254 220,128 0"></polygon>
                                </svg>
                                <!-- End Triangle Dotted -->
                                <div class="triangle-img-warp tri">
                                    <img src="images\Whychooseus\1.webp" class="img-responsive" alt="Image">
                                </div>
                            </div>
                        </div>

                        <!-- End -->

                        <div class="chooseus-item">
                            <a href="#"><h4 class="text-cap">Experiencia</h4></a>
                            <div class="chooseus-canvas-item">
                                <svg class="svg-hexagon">
                                    <polygon class="hexagon"
                                             points="285 100,285 250,155 325,25 250,25 100,155 25"></polygon>
                                </svg>
                                <!-- End Hexagon -->
                                <svg class="svg-triangle-dotted svg-tri-2">
                                    <polygon class="triangle-div" points="2 220,254 220,128 0"></polygon>
                                </svg>
                                <!-- End Triangle Dotted -->
                                <div class="triangle-img-warp tri2">
                                    <img src="images\Whychooseus\2.webp" class="img-responsive" alt="Image">
                                </div>
                            </div>
                        </div>

                        <!-- End -->

                        <div class="chooseus-item mgb0">
                            <a href="#"><h4 class="text-cap">Limpieza</h4></a>
                            <div class="chooseus-canvas-item">
                                <svg class="svg-hexagon">
                                    <polygon class="hexagon"
                                             points="285 100,285 250,155 325,25 250,25 100,155 25"></polygon>
                                </svg>
                                <!-- End Hexagon -->
                                <svg class="svg-triangle-dotted svg-tri-3">
                                    <polygon class="triangle-div" points="2 220,254 220,128 0"></polygon>
                                </svg>
                                <!-- End Triangle Dotted -->
                                <div class="triangle-img-warp tri3">
                                    <img src="images\Whychooseus\3.webp" class="img-responsive" alt="Image">
                                </div>
                            </div>
                        </div>

                        <!-- End -->

                        <div class="chooseus-item mgb0">
                            <a href="#"><h4 class="text-cap">Modernidad</h4></a>
                            <div class="chooseus-canvas-item">
                                <svg class="svg-hexagon">
                                    <polygon class="hexagon"
                                             points="285 100,285 250,155 325,25 250,25 100,155 25"></polygon>
                                </svg>
                                <!-- End Hexagon -->
                                <svg class="svg-triangle-dotted svg-tri-4">
                                    <polygon class="triangle-div" points="2 220,254 220,128 0"></polygon>
                                </svg>
                                <!-- End Triangle Dotted -->
                                <div class="triangle-img-warp tri4">
                                    <img src="images\Whychooseus\4.webp" class="img-responsive" alt="Image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section Why Choose Us -->

        <section class="padding bg-grey padding-bottom-0">
            <div class="title-block">
                <h2 class="title text-cap">PRODUCTOS</h2>
                <h4 class="title text-cap">Manejamos productos de la más alta calidad para proporcionarte las mejores
                    opciones en pisos, persianas, alfombras, tapices y accesorios, además de contar con diferentes
                    estilos para decorar cada espacio conforme a tu gusto.</h4>
                <div class="divider divider-1">
                    <svg class="svg-triangle-icon-container">
                        <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                    </svg>
                </div>
            </div>
            <!-- End Title -->
            <div class="lastest-project-warp clearfix">
                <div class="projectFilter project-terms line-effect-2">
                    <a href="#" data-filter="*" class="current text-cap"><h4>Todos</h4></a>
                    <a href="#" data-filter=".Residential" class="text-cap"><h4>Persianas</h4></a>
                    <a href="#" data-filter=".Ecommercial" class="text-cap"><h4>Modelos</h4></a>
                    <a href="#" data-filter=".Office" class="text-cap"><h4>Geometrías</h4></a>
                    <a href="#" data-filter=".Hospital" class="text-cap"><h4>Pisos</h4></a>
                    <a href="#" data-filter=".Alfombras" class="text-cap"><h4>Alfombras</h4></a>
                    <a href="#" data-filter=".Tapiz" class="text-cap"><h4>Tapíz</h4></a>
                </div> <!-- End Project Fillter -->

                <div class="clearfix projectContainer">

                    <div class="element-item  Residential">

                        <img src="images\Services\cortinas.webp" style="height: 284px;" class="img-responsive"
                             alt="Image">
                        <div class="project-info">
                            <a href="productos"><h2 class="title-project text-cap text-cap white-text">
                                    Persianas</h2></a>
                            <a href="productos" class="cateProject">Conoce más</a>
                        </div>
                    </div>


                    <div class="element-item Ecommercial">
                        <a class="img-contain-isotope" href="productos">
                            <img src="images\Project\3.webp" class="img-responsive" alt="Image">
                        </a>
                        <div class="project-info">
                            <a href="productos"><h2 class="title-project text-cap white-text">Modelos</h2>
                            </a>
                            <a href="productos" class="cateProject">Conoce más</a>
                        </div>
                    </div>


                    <div class="element-item Office">
                        <a class="img-contain-isotope" href="productos">
                            <img src="images\Project\5.webp" class="img-responsive" alt="Image">
                        </a>
                        <div class="project-info">
                            <a href="productos"><h2 class="title-project text-cap white-text">GEOMETRÍAS</h2>
                            </a>
                            <a href="productos" class="cateProject">Conoce más</a>
                        </div>
                    </div>


                    <div class="element-item Hospital ">
                        <a class="img-contain-isotope" href="productos">
                            <img src="images\Services\piso.webp" style="height: 284px;" class="img-responsive"
                                 alt="Image">
                        </a>
                        <div class="project-info">
                            <a href="productos"><h2 class="title-project text-cap white-text">PISOS</h2></a>
                            <a href="productos" class="cateProject">Conoce más</a>
                        </div>
                    </div>


                    <div class="element-item">
                        <a class="img-contain-isotope" href="productos">
                            <img src="images\Project\7.webp" class="img-responsive" alt="Image">
                        </a>
                        <div class="project-info">
                            <a href="productos"><h2 class="title-project text-cap white-text">CONOCE MÁS</h2>
                            </a>
                        </div>
                    </div>

                    <div class="element-item">
                        <a class="img-contain-isotope" href="productos">
                            <img src="images\Services\img1.webp" style="height: 284px;" class="img-responsive"
                                 alt="Image">
                        </a>
                        <div class="project-info">
                            <a href="productos"><h2 class="title-project text-cap white-text">CONOCE MÁS</h2>
                            </a>
                        </div>
                    </div>

                    <div class="element-item Alfombras ">
                        <a class="img-contain-isotope" href="productos">
                            <img src="images\Services\alfom.webp" style="height: 284px; width: 100%;"
                                 class="img-responsive" alt="Image">
                        </a>
                        <div class="project-info">
                            <a href="productos"><h2 class="title-project text-cap white-text">ALFOMBRAS</h2>
                            </a>
                            <a href="productos" class="cateProject">Conoce más</a>
                        </div>
                    </div>


                    <div class="element-item Tapiz ">
                        <a class="img-contain-isotope" href="productos">
                            <img src="images\Services\tapiz.webp" style="height: 284px;" class="img-responsive"
                                 alt="Image">
                        </a>
                        <div class="project-info">
                            <a href="productos"><h2 class="title-project text-cap white-text">TAPIZ</h2></a>
                            <a href="productos" class="cateProject">Conoce más</a>
                        </div>
                    </div>


                </div>  <!-- End project Container -->
            </div> <!-- End  -->
            <div class="overlay-arc">
                <div class="layer-1">
                    <a href="productos" class="ot-btn btn-dark-color text-cap">Ver todos los productos</a>
                </div>
            </div>
        </section>
        <!-- End Section Isotop Lastest Project -->
        <!--
        <section class="padding ">
            <div class="container">
            <div class="row">
                <div class="title-block">
                    <h2 class="title text-cap">Lastest From News</h2>
                    <div class="divider divider-1">
                        <svg class="svg-triangle-icon-container">
                          <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                        </svg>
                    </div>
                </div>
                <div class="lastest-blog-container">
                    <div class="col-md-6">
                        <article class="lastest-blog-item">
                            <figure class="latest-blog-post-img effect-zoe">
                                <a href="blogDetail">
                                    <img src="images\Blog\1.jpg" class="img-responsive" alt="Image">
                                </a>
                                <div class="latest-blog-post-date text-cap">
                                    <span class="day">21</span>
                                    <span class="month">May</span>
                                </div>
                            </figure>
                            <div class="latest-blog-post-description">
                                <a href="blogDetail"><h3>2016 Interior Design Trends</h3></a>
                                <p>Morbi vehicula a nibh in commodo. Aliquam quis dolor eget lectus pulvinar eu rhoncus ligula. Ut leo mauris, molestie imperdiet consequat in, varius ac sapien.</p>

                                <a href="blogDetail" class="ot-btn btn-main-color text-cap mgb0">
                                    Continue Reading...
                                </a>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-6  ">
                        <article class="lastest-blog-item">
                            <figure class="latest-blog-post-img effect-zoe">
                                <a href="blogDetail">
                                    <img src="images\Blog\2.jpg" class="img-responsive" alt="Image">
                                </a>
                                <div class="latest-blog-post-date text-cap">
                                    <span class="day">18</span>
                                    <span class="month">May</span>
                                </div>

                            </figure>
                            <div class="latest-blog-post-description">
                                <a href="blogDetail"><h3>15 Notable Products at ARC Interior Design Contest</h3></a>
                                <p>Morbi vehicula a nibh in commodo. Aliquam quis dolor eget lectus pulvinar eu rhoncus ligula. Ut leo mauris, molestie imperdiet consequat in, varius ac sapien.</p>

                                <a href="blogDetail" class="ot-btn btn-main-color text-cap mgb0">
                                    Continue Reading...
                                </a>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
            </div>
        </section>-->
        <!-- End Section Lastest Blog -->

        <!--<section class="padding bg-grey">
            <div class="container">
                <div class="row">
                    <div class="title-block">
                        <h2 class="title text-cap">Clientes</h2>
                        <div class="divider divider-1">
                            <svg class="svg-triangle-icon-container">
                                <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-partner-warp">
                        <div class="customNavigation">
                            <a class="btn prev-partners"><i class="fa fa-angle-left"></i></a>
                            <a class="btn next-partners"><i class="fa fa-angle-right"></i></a>
                        </div>

                        <div id="owl-partners" class="owl-carousel owl-theme owl-partners clearfix">
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\1.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\2.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\3.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\4.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\5.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\1.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\2.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\3.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\4.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images\Partner\5.png" class="img-responsive" alt="Image">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        -->
        <!-- End Section Owl Partners -->

        <!--
        <section class="padding bg-parallax section-dark-testimonials">
            <div class="container">
            <div class="row">
                <div class="title-block">
                    <h2 class="title text-cap"></h2>
                    <div class="divider divider-2">
                        <svg class="svg-triangle-icon-container">
                          <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                        </svg>
                    </div>
                </div>
                <div class="testimonial-warp testimonial-2-col">
                      <div class="customNavigation">
                        <a class="btn prev-testimonials-2-columns"><i class="fa fa-angle-left"></i></a>
                        <a class="btn next-testimonials-2-columns"><i class="fa fa-angle-right"></i></a>
                      </div>
                      <div id="owl-testimonials-2-columns" class="owl-carousel owl-theme clearfix">
                      <div class="item item-testimonials text-left">
                          <p class="quote-icon">“</p>
                          <p><i>Morbi auctor vel mauris facilisis lacinia. Aenean suscipit lorem leo, et hendrerit odio fermentum et. Donec ac dolor eros. Mauris arcu nunc, iaculis sit amet lacus iaculis, faucibus faucibus nunc. Vestibulum sit amet lacinia massa</i></p>
                          <div class="avatar-testimonials">
                            <img src="images\Testimonials\1.jpg" class="img-responsive" alt="Image">
                          </div>
                          <h4 class="name-testimonials text-cap">Linda Campbell</h4>
                          <span class="job-testimonials">CEO Finanace Theme Group</span>
                      </div>
                      <div class="item item-testimonials text-left">
                          <p class="quote-icon">“</p>
                          <p><i>Morbi auctor vel mauris facilisis lacinia. Aenean suscipit lorem leo, et hendrerit odio fermentum et. Donec ac dolor eros. Mauris arcu nunc, iaculis sit amet lacus iaculis, faucibus faucibus nunc. Vestibulum sit amet lacinia massa</i></p>
                          <div class="avatar-testimonials">
                            <img src="images\Testimonials\2.jpg" class="img-responsive" alt="Image">
                          </div>
                          <h4 class="name-testimonials text-cap">John Walker</h4>
                          <span class="job-testimonials">Photographer</span>
                      </div>
                      <div class="item item-testimonials text-left">
                          <p class="quote-icon">“</p>
                          <p><i>Morbi auctor vel mauris facilisis lacinia. Aenean suscipit lorem leo, et hendrerit odio fermentum et. Donec ac dolor eros. Mauris arcu nunc, iaculis sit amet lacus iaculis, faucibus faucibus nunc. Vestibulum sit amet lacinia massa</i></p>
                          <div class="avatar-testimonials">
                            <img src="images\Testimonials\3.jpg" class="img-responsive" alt="Image">
                          </div>
                          <h4 class="name-testimonials text-cap">Cheryl Cruz</h4>
                          <span class="job-testimonials">Marketing Manager</span>
                      </div>
                      <div class="item item-testimonials text-left">
                          <p class="quote-icon">“</p>
                          <p><i>Morbi auctor vel mauris facilisis lacinia. Aenean suscipit lorem leo, et hendrerit odio fermentum et. Donec ac dolor eros. Mauris arcu nunc, iaculis sit amet lacus iaculis, faucibus faucibus nunc. Vestibulum sit amet lacinia massa</i></p>
                          <div class="avatar-testimonials">
                            <img src="images\Testimonials\4.jpg" class="img-responsive" alt="Image">
                          </div>
                          <h4 class="name-testimonials text-cap">James Smith</h4>
                          <span class="job-testimonials">Senior Finance Manager</span>
                      </div>
                      <div class="item item-testimonials text-left">
                          <p class="quote-icon">“</p>
                          <p><i>Morbi auctor vel mauris facilisis lacinia. Aenean suscipit lorem leo, et hendrerit odio fermentum et. Donec ac dolor eros. Mauris arcu nunc, iaculis sit amet lacus iaculis, faucibus faucibus nunc. Vestibulum sit amet lacinia massa</i></p>
                          <div class="avatar-testimonials">
                            <img src="images\Testimonials\5.jpg" class="img-responsive" alt="Image">
                          </div>
                          <h4 class="name-testimonials text-cap">Maria Garcia</h4>
                          <span class="job-testimonials">Finance Director Theme Group</span>
                      </div>
                      <div class="item item-testimonials text-left">
                          <p class="quote-icon">“</p>
                          <p><i>Morbi auctor vel mauris facilisis lacinia. Aenean suscipit lorem leo, et hendrerit odio fermentum et. Donec ac dolor eros. Mauris arcu nunc, iaculis sit amet lacus iaculis, faucibus faucibus nunc. Vestibulum sit amet lacinia massa</i></p>
                          <div class="avatar-testimonials">
                            <img src="images\Testimonials\6.jpg" class="img-responsive" alt="Image">
                          </div>
                          <h4 class="name-testimonials text-cap">Robert Johnson</h4>
                          <span class="job-testimonials">Finance Assistant - PR Agency</span>
                      </div>
                      </div>
                  </div>
            </div>
            </div>
        </section>-->
        <!-- End Section Owl Testimonials -->

        <!--
        <section class="padding ">
            <div class="container">
            <div class="row">
                <div class="title-block">
                    <h2 class="title text-cap">Don't forget to sign up!</h2>
                    <div class="divider divider-1">
                        <svg class="svg-triangle-icon-container">
                          <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                        </svg>
                    </div>
                </div>
                <div class="form-subcribe">

                    <p class="text-center">Find out early nosotros all upcoming promotions and new products releases with<br> our newsletter.</p>
                    <form method="post">
                        <input class="newsletter-email input-text" placeholder="email@example.com" type="email">
                        <button class="ot-btn btn-main-color text-cap" type="submit">Subscribe</button>
                    </form>
                </div>
            </div>
            </div>
        </section>
        -->
        <!-- End Section subcribe -->

        <footer class="footer-v1">
            <div class="footer-left">
                <a href="index">
                    <h4 style="color: white;"><span><strong>DISTRIBUIDORA DE PISOS</strong></span></h4>
                    <!--<img src="images\logotipo.gif" style="width: 150%;" class="img-responsive" alt="Image">-->
                </a>
            </div>
            <!-- End Left Footer -->
            <nav>
                <ul>
                    <li>
                        <a class="text-cap" href="index">INICIO</a>
                    </li>
                    <li>
                        <a class="text-cap" href="productos">PRODUCTOS</a>
                    </li>
                    <li>
                        <a class="text-cap" href="servicios">SERVICIOS</a>
                    </li>
                    <li>
                        <a class="text-cap" href="nosotros">¿Quienes somos?</a>
                    </li>
                    <li>
                        <a class="text-cap" href="contacto">contactanos</a>
                    </li>
                </ul>
            </nav>
            <!-- End Nav Footer -->
            <div class="footer-right">
                <ul class="social social-footer">
                    <li class="facebook active"><a><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a><i class="fa fa-google-plus"></i></a></li>
                    <li class="youtube"><a><i class="fa fa-youtube-play"></i></a></li>
                    <li class="linkedin"><a><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <!-- End Right Footer -->
        </footer>
        <!-- End Footer -->
        <?php
        include 'conexion/conexion.php';

        $promo = "SELECT Promo_text FROM promocion";
        //echo $id_user;
        $consultaid = mysqli_query($mysqli, $promo);
        $user_id = mysqli_fetch_assoc($consultaid);
        $id = $user_id['Promo_text'];

        if($id != ""){
            ?>
            <!-- Modal de promoción-->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">INFORMACIÓN DEL SITIO</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?= "<h3>".$id."</h3>";?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }


        ?>




        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css\bootstrap.min.css">
        <!-- Font -->
        <link rel="stylesheet" href="css\font-awesome.min.css">
        <link rel="stylesheet" href="css\elegant-font.css">

        <!-- REVOLUTION STYLE SHEETS -->
        <link rel="stylesheet" type="text/css" href="revolution\css\settings.css">
        <!-- REVOLUTION LAYERS STYLES -->
        <link rel="stylesheet" type="text/css" href="revolution\css\layers.css">
        <!-- REVOLUTION NAVIGATION STYLES -->
        <link rel="stylesheet" type="text/css" href="revolution\css\navigation.css">
        <!-- OWL CAROUSEL
          ================================================== -->
        <link rel="stylesheet" href="css\owl.carousel.css">
        <!-- SCROLL BAR MOBILE MENU
        ================================================== -->
        <link rel="stylesheet" href="css\jquery.mCustomScrollbar.css">

        <!-- color scheme -->
        <link rel="stylesheet" href="switcher\demo.css" type="text/css">
        <link rel="stylesheet" href="switcher/colors/maroon.css" type="text/css" id="colors">

        <section class="copyright">
            <p>Copyright © 2020 Designed by <a href="esquitechs.com" style="color: deepskyblue;">Esquitechs</a>.
                All rights reserved.</p>
        </section>
    </div>
</div>
<!-- End page -->

<a id="to-the-top"><i class="fa fa-angle-up"></i></a>
<!-- Back To Top -->


<!-- SCRIPT -->
<script src="js\vendor\jquery.min.js"></script>
<script src="js\vendor\bootstrap.min.js"></script>
<script src="js\plugins\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js\plugins\wow.min.js"></script>
<script type="text/javascript" src="js\plugins\skrollr.min.js"></script>
<!-- Switcher
================================================== -->
<script src="switcher\demo.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="revolution\js\jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="revolution\js\jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
    (Load Extensions only on Local File Systems !
    The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="revolution\js\extensions\revolution.extension.video.min.js"></script>
<!-- Intializing Slider-->
<script type="text/javascript" src="js\plugins\slider.js"></script>

<!-- Mobile Menu
================================================== -->
<script src="js\plugins\jquery.mobile-menu.js"></script>

<!-- Initializing the isotope
================================================== -->
<script src="js\plugins\isotope.pkgd.min.js"></script>
<script src="js\plugins\custom-isotope.js"></script>
<!-- Initializing Owl Carousel
================================================== -->
<script src="js\plugins\owl.carousel.js"></script>
<script src="js\plugins\custom-owl.js"></script>


<!-- PreLoad
================================================== -->
<!--<script type="text/javascript" src="js\plugins\royal_preloader.min.js"></script>
<script type="text/javascript">
    (function ($) {
        "use strict";
        Royal_Preloader.config({
            mode: 'logo',
            logo: 'images/logotipo.gif',
            timeout: 1,
            showInfo: false,
            opacity: 1,
            background: ['#FFFFFF']
        });
    })(jQuery);
</script>
-->|
<!-- Global Js
================================================== -->
<script src="js\plugins\custom.js"></script>

<!--promo-->
<script>
    $( document ).ready(function() {
        $('#exampleModalCenter').modal('toggle')
    });
</script>

</body>
</html>
