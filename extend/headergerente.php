<?php
include '../conexion/conexion.php';

if (!isset($_SESSION['id']) || !isset($_SESSION['name']) || !isset($_SESSION['mail'])) {
    header('location: ../');
//cuando se ingresa del login se general las variables de sesion desde el login, aqui si existe se redirecciona a inicio
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Facturacion cliente</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="icon" href="../img/logo/logo/login-logo.png"/>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/mdb.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>

<header style="padding-bottom: 100px;">
    <!--comienza loader-->
    <div class="loader"></div>
    <style>
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('../img/dash.gif') 50% 50% no-repeat rgb(249,249,249);
            opacity: .8;
        }
    </style>
    <style>
        body{
            padding:20px 20px;
        }

        .results tr[visible='false'],
        .no-result{
            display:none;
        }

        .results tr[visible='true']{
            display:table-row;
        }

        .counter{
            padding:8px;
            color:white;
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>
    <!-- termina loader-->
    <nav class="navbar fixed-top navbar-expand-md navbar-light black white-text double-nav scrolling-navbar top-nav-collapse">
        <!-- SideNav slide-out button -->
        <div class="float-left">
            <a class="navbar-brand " href="#"><img src="../img/logo/logo/login-logo.png" style="width: 40px;"
                                                   href="home"/></a>

        </div>
        <!--Navigation icons-->
        <ul class="nav navbar-nav nav-flex-icons ml-auto">
            <li class="nav-item">
                <a class="nav-link text-white" href="../gerencia/index"><i class="far fa-keyboard"></i>Agregar<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="../gerencia/pensions"><i class="fas fa-car-side"></i>Pensiones</a>
            </li>

            <li class="nav-item ">
                <a class="nav-link">
                    <span class="text-white" style="font-size: small;">Usuario: <?= $_SESSION['name']; ?> </span>

                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white btn-primary btn-rounded aqua-gradient" href="../login/Salir"><span style="font-size: small;">Cerrar Sesión</span></a>
            </li>

    </nav>


</header>
<main class="py-4">
    <body>



    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <script src="js/checkbox.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/0.0.11/push.min.js"></script>-->
    <script>
        /*
        Push.Permission.request();
        Push.create('Hi there!', {
            body: 'This is a notification.',
            icon: 'icon.png',
            timeout: 8000,               // Timeout before notification closes automatically.
            vibrate: [100, 100, 100],    // An array of vibration pulses for mobile devices.
            onClick: function() {
                // Callback for when the notification is clicked.
                console.log(this);
            }
        });*/
    </script>
    </body>
</html>
