<?php  header ('Content-type: text/html; charset=utf-8');?>
<!DOCTYPE html><html lang="es" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.css">
    <title>Facturador</title>
</head>
<body>
<?php
$mensaje = strip_tags(htmlentities($_GET['msj']));
$c = strip_tags(htmlentities($_GET['c']));
$p = strip_tags(htmlentities($_GET['p']));
$t = strip_tags(htmlentities($_GET['t']));
//


switch ($c) {
    case 'index':
        $carpeta = '../';
        break;
    case 'menu':
        $carpeta = '../menu/';
        break;

    case 'gerencia':
        $carpeta = '../gerencia/';
        break;

}//termina switch

switch ($p) {
    case 'index':
        $pagina = 'index';
        break;

    case 'gerentes':
        $pagina = 'gerente';
        break;

    case 'menu':
        $pagina = 'manual';
        break;

}//termina segundo switch

$dir = $carpeta.$pagina;
if ($t == "error") {
    $titulo = "Oppss...";
}else{
    $titulo = "Buen trabajo";
}

?>

<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
<script>
    swal({
        title: '<?php echo $titulo ?>',
        text: '<?php echo $mensaje ?>',
        type: '<?php echo $t ?>',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'ok'
    }).then(function(){
        location.href='<?php echo $dir ?>';
    });

    $(document).click(function(){
        location.href='<?php echo $dir ?>';
    });

    $(document).keyup(function(e){
        if (e.which == 27) {
            location.href='<?php echo $dir ?>';
        }
    });

</script>

</body>
</html>
